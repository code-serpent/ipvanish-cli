'''
Collection of tools for __main__ to querry quickvpn and prepare data.
'''
import os
import sys
import glob
from pathlib import Path
import zipfile
import io
import json
import operator
import requests

def get_external_ip():
    """
    This function will test your external IP and return it as a string.
    """
    ip_test_website = 'http://checkip.amazonaws.com/'

    #Gather public IP
    try:
        current_ip = requests.get(ip_test_website).text.strip()
    except requests.exceptions.ConnectionError:
        print('internet error')
        current_ip = "0.0.0.0"

    return current_ip

def convert_server_fqdn_to_path(servername='something.ipvanish.com'):
    '''Converts Servername into file path to .ovpn file.'''
    change_dir_to_ipvanish_dir(create=False)

    server_basename = servername.split('.')[0]
    config_name = server_basename + '.ovpn'
    final_name = './*' + config_name
    try:
        ovpn_file = glob.glob(final_name)[0]
    except IndexError:
        #updateConfigs()
        try:
            ovpn_file = glob.glob(final_name)[0]
        except IndexError:
            print('Cannot find config for {}'.format(server_basename))
            print('Try running the -d, --download flag first.')
            sys.exit(1)

    return ovpn_file

def change_dir_to_ipvanish_dir(create=False):
    '''cd to $HOME/.IPVanish/'''
    path = Path.home()
    config_dir = path / '.IPVanish'
    if create:
        try:
            os.chdir(str(config_dir))
        except FileNotFoundError:
            os.mkdir(str(config_dir))
            os.chdir(str(config_dir))
    else:
        try:
            os.chdir(str(config_dir))
        except FileNotFoundError:
            print('Please donwload the configs by running the -d, --download option/flag')
            sys.exit(1)
    


def download_configs():
    '''Downloads configs into configs directory.'''

    url = 'https://www.ipvanish.com/software/configs/configs.zip'
    change_dir_to_ipvanish_dir(create=True)

    try:
        url_stream = requests.get(url)
    except requests.exceptions.ConnectionError:
        print("Issue downloading the configs")
        sys.exit()
    zip_file = zipfile.ZipFile(io.BytesIO(url_stream.content))
    zip_file.extractall()
    print('Configs downloaded successfully')

def get_servers():
    '''Show list of IPVanish server's'''

    url = 'https://www.ipvanish.com/api/servers.geojson'

    server_list = json.loads(requests.get(url).content)
    servers_clean = []
    for server in server_list:
        city = server['properties']['city']
        country = server['properties']['country']
        hostname = server['properties']['hostname']
        capacity = server['properties']['capacity']
        online = server['properties']['online']

        servers_clean.append([country, city, hostname, capacity, online])

    sorted_servers = sorted(servers_clean, key=operator.itemgetter(0, 1, 3))

    return sorted_servers
