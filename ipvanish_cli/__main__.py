'''
Handles the main logic of the CLI interface.
'''
import os
import sys
import operator
import argparse
import tempfile
import keyring
from tabulate import tabulate
from . import quickvpn
from . import tools
ABS_PATH = os.path.abspath(__file__)
DIR_NAME = os.path.dirname(ABS_PATH)
os.chdir(DIR_NAME)

def get_args():
    '''Creates the CLI'''

    about = '''Launches openvpn client to IPVansish fqdn or if city and/or
               country are used, lowest capacity server is slected from group.'''
    parser = argparse.ArgumentParser(description=about, prog='ipvanish-cli')
    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument(
        '-l',
        '--login',
        action='store_true',
        help='prompts users for login info and is then saved in login.conf.',
        )
    group.add_argument(
        '-i',
        '--identify',
        action='store_true',
        help='Returns current external IP. Query http://checkip.amazonaws.com/',
        )
    group.add_argument(
        '-d',
        '--download',
        action='store_true',
        help='Downloads configs from ipvanish.',
        )
    group.add_argument(
        '-r',
        '--report',
        action='store_true',
        help='''Display list IPVanish servers - Use city and
                country flags to narrow scope''',
        )
    group.add_argument(
        '-k',
        '--kill',
        action='store_true',
        help='Kills any running openvpn process',
        )
    parser.add_argument(
        '-f',
        '--fqdn',
        action='store',
        help='Selects server with fqdn',
        )
    parser.add_argument(
        '--city',
        action='store',
        help='Select all servers from city',
        )
    parser.add_argument(
        '--country',
        action='store',
        help='Select all servers from country',
        )
    group.add_argument(
        '-c',
        '--connect',
        action='store_true',
        help='Connects to selected server with lowest capacity (AKA least used server). If fqdn is provided, connect there',
        )


    args = parser.parse_args()

    return(
        args.kill,
        args.download,
        args.report,
        args.identify,
        args.fqdn,
        args.city,
        args.country,
        args.connect,
        args.login,
        )

def kill_true():
    '''If kill var is True, kill running openvpn processes'''
    quickvpn.kill_openvpn_process()
    sys.exit()

def identify_true():
    '''If identify var is True, print current external IP address'''
    print(tools.get_external_ip())
    sys.exit()

def download_true():
    '''If download var is True, download configs from IPVanish website.'''
    tools.download_configs()
    sys.exit()

def build_country_list(server_list, country=None):
    '''Function to build country list'''

    country_list = []
    if country is not None:
        for server in server_list:
            if server[0] == country:
                country_list.append(server)

        if not country_list:
            print("Country:", country)
            print("Not Found")
            sys.exit(1)

    return country_list

def build_city_list(server_list, city=None):
    '''Function to build city list'''
    city_list = []
    if city is not None:
        for server in server_list:
            # Removes hostnames with '-c' in hostname.
            # Usually a cert is not availbile
            if "-c" in server[2]:
                continue

            if server[1] == city:
                city_list.append(server)

        if not city_list:
            print("City:", city)
            print("Not Found")
            sys.exit(1)

    return city_list

def connect_vpn(fqdn='something.ipvanish.com'):
    '''Starts a openvpn session to fqdn'''
    original_external_ip = tools.get_external_ip()
    ovpn_file = tools.convert_server_fqdn_to_path(servername=fqdn)
    try:
        with open('login.conf', 'r') as username_file:
            username = username_file.readline().strip()
    
    except FileNotFoundError:
        print('Please provide the login info to IPvanish account. Run with -l or --login')
        sys.exit(0)

    password = keyring.get_password('IPVanish', username)
    with tempfile.NamedTemporaryFile(mode='r+b', delete=True) as temp_file:
        temp_file.write(str(username).encode('utf8'))
        temp_file.write(str('\n').encode('utf8'))
        temp_file.write(str(password).encode('utf8'))
        temp_file.seek(0)

        pid = quickvpn.start_process(ovpn_file=ovpn_file, login_file=temp_file.name)

    new_external_ip = tools.get_external_ip()

    if new_external_ip == '0.0.0.0':
        print('Internet Error')
        sys.exit(-1)

    if new_external_ip == original_external_ip:
        print('External IP did not changed.\nOriginal IP =\t {0}\n'.format(original_external_ip))
        print('try using sudo')
        sys.exit(-1)
    else:
        print('External IP changed.')
        print(f'Original IP =\t {original_external_ip}')
        print(f'New IP \t=\t{new_external_ip }\n\n\n')

        print('To kill this process copy the following line.')
        print('sudo kill {}\n\n'.format(pid))
        sys.exit(0)

def report_gen(server_list=()):
    '''Generates report'''

    try:
        print(tabulate(server_list, headers=['Country', 'City', 'fqdn', 'Capacity', 'Online']))
        sys.stdout.flush()
    except BrokenPipeError:
        # Python flushes standard streams on exit; redirect remaining output
        # to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        sys.exit(1)  # Python exits with error code 1 on EPIPE



def main():
    '''
    Gathers the CLI args. Determines which mode to run in. Builds list for
    city, country, and all servers provided by IPVanish. Then either generates
    a report or connects to the lowest capacity server selected.
    '''


    kill, download, report, identify, fqdn, city, country, connect, login = get_args()

    if login:
        status = quickvpn.login_true()
        sys.exit(status)
    if download:
        download_true()

    if kill:
        kill_true()

    if identify:
        identify_true()

    city_list = []
    country_list = []
    server_list = tools.get_servers()
    if fqdn is None:
        city_list = build_city_list(server_list, city=city)
        country_list = build_country_list(server_list, country=country)
    else:
        for server in server_list:
            if server[2] == fqdn:
                city_list.append(server)
                break
        else:
            print('fqdn: ', fqdn, 'is not found. Please double check with -l --list option.')
            sys.exit(1)

    # Removing duplicates from country_list if found in city_list
    clean_country_list = []
    for server in country_list:
        for server1 in city_list:
            if server == server1:
                break
        else:
            clean_country_list.append(server)

    city_country_list = [*city_list, *clean_country_list]

    # Sorts final lists to be ordered by Capacity, city, then country.
    city_country_list = sorted(city_country_list, key=operator.itemgetter(3, 1, 0))
    server_list = sorted(server_list, key=operator.itemgetter(3, 1, 0))

    if city_country_list:
        fqdn = city_country_list[0][2]
    else:
        fqdn = server_list[0][2]

    print('fqdn:', fqdn)

    if report:
        if not city_list and not country_list:
            report_gen(server_list=server_list)
        else:
            report_gen(server_list=city_country_list)
        sys.exit(0)

    if connect:
        connect_vpn(fqdn=fqdn)

if __name__ == '__main__':
    main()
