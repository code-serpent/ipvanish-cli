#!/usr/bin/env python3
'''
module for quickly spawning off a openvpn serssion
'''
### Quick VPN ###
import os
import getpass
from pathlib import Path
from . import tools
from fcntl import fcntl, F_SETFL
from subprocess import Popen, PIPE
from time import sleep
import keyring


def start_process(ovpn_file='Path_to_file', login_file='Path_to_file'):
    """
    This function will start the openvpn proccess in the backgroud.
    Returns PID number.
    """
    tools.change_dir_to_ipvanish_dir(create=False)
    open_vpn_cmd = ['sudo', '/usr/sbin/openvpn', '--auth-nocache', '--config', ovpn_file,
                    '--auth-user-pass', login_file, '--ca',
                    'ca.ipvanish.com.crt', ]
    session = Popen(open_vpn_cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=False)
    # set the O_NONBLOCK flag of session.stdout file descriptor:
    fcntl(session.stdout, F_SETFL, os.O_NONBLOCK)
    # get the output
    sleep(5)
    try_again = True
    limit = 5
    output = ''
    while try_again:
        try:
            sleep(1)
            output = output + str(os.read(session.stdout.fileno(), 2048))
        except OSError:
            # the os throws an exception if there is no data
            limit = limit - 1

        if limit <= 0:
            try_again = False
        else:
            limit = limit - 1

    for i in output.split(r'\n'):
        print(i)
    return session.pid

def kill_openvpn_process():
    '''
    Kills openvpn process that is running. Equal to the following bash oneliner but with more
    error checking:
    $ sudo kill $(ps aux | grep -i 'openvpn --auth-nocache' | head -n 1 | cut -d' ' -f 6)
    '''

    process_1 = Popen(['ps', 'aux'], stdout=PIPE, universal_newlines=True)
    process_2 = Popen(['grep', '-i', 'openvpn --auth-nocache'],
                      stdin=process_1.stdout, stdout=PIPE, universal_newlines=True)
    process_1.stdout.close() # Allow process_1 to receive a SIGPIPE if process_2 exits.
    output = process_2.communicate()[0]
    output = output.splitlines()
    for num, row in enumerate(output):
        output[num] = row.split()
    if output[0][10] == 'grep':
        print('No openvpn processes running. Exitting...')
        return

    open_pid = output[0][1]


    process_3 = Popen(['sudo', 'kill', open_pid], universal_newlines=True)
    process_3.wait()
    process_3.poll()
    ret_code = process_3.returncode
    if ret_code != 0:
        print('failed to close {}. Please check process'.format(open_pid))
        print(output)
    else:
        print('Closed {}.'.format(open_pid))


def login_true():
    '''If login var is True, proccess username and password.'''
    username = input('Username: ')
    keyring.set_password('IPVanish', username, getpass.getpass('Password: '))
    tools.change_dir_to_ipvanish_dir(create=True)
    with open('login.conf', 'w+') as login_file:
        login_file.write(username)
        login_file.write('\n')
    return True
