#!/usr/bin/env python3
import os
import ipaddress
import glob
from pathlib import Path
import pytest
from ipvanish_cli import tools

abs_path = os.path.abspath(__file__)
dir_name = os.path.dirname(abs_path)
dir_name = os.path.dirname(dir_name)
dir_name = os.path.join(dir_name, "ipvanish_cli")
os.chdir(dir_name)

def test_get_external_ip():
    '''Tests tools.get_external_ip() returns a vaild IPv4 address'''
    ip_address = tools.get_external_ip()
    ipaddress.ip_address(ip_address)

def test_convert_server_fqdn_to_path():
    '''Tests if convertServernameToPath returns expected path string'''

    path = tools.convert_server_fqdn_to_path(servername='dal-a24.ipvanish.com')
    assert str(path) == r'./ipvanish-US-Dallas-dal-a24.ovpn'

    #test to see if convert_server_fqdn_to_path fails correctly.
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        path = tools.convert_server_fqdn_to_path(servername='something.ipvanish.com')
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 1


def test_change_dir_to_ipvanish_dir():
    '''Test that the path was changed correctly.'''
    tools.change_dir_to_ipvanish_dir()
    assert str(Path.home()) in str(os.getcwd())

def test_download_configs():
    '''Test to ensure ovpn files were downloaded from IPVanish.'''
    tools.download_configs()
    print(os.getcwd())
    ovpn_file_list = glob.glob('./*.ovpn')
    assert len(ovpn_file_list) > 0

def test_get_servers():
    '''Test to ensure that a list of servers is returned'''
    server_list = tools.get_servers()

    assert len(server_list) > 100
    assert 'ipvanish.com' in server_list[0][2]
