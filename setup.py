import setuptools

# read the contents of your README file
from os import path
this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setuptools.setup(
    name="ipvanish-cli",
    version="2.0.1",
    author="Anthony Aguilar",
    author_email="Anthony.Aguilar@code-serpent.com",
    description="Tool to quickly connect to a IPVanish vpn session",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://code-serpent@bitbucket.org/code-serpent/ipvanish-cli.git",
    packages=setuptools.find_packages(),
    package_dir={'ipvanish_cli':'ipvanish_cli'}, # the one line where all the magic happens
    package_data={'ipvanish_cli':['ipvanish_cli/configs/*'],},
    entry_points={
    'console_scripts': [
        'ipvanish-cli=ipvanish_cli.__main__:main',
    ],
    },
    include_package_data=True,
    install_requires=[
   'requests==2.18.4',
   'tabulate==0.8.2',
   'keyring==19.0.1',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU v3 License",
        "Operating System :: Fedora",
    ],
)
