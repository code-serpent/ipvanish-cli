Only supports openvpn and IPvanish. This was devolped on my Fedora 29 Laptop with python 3.7.

You can use this tool in a few different ways.

```bash
$ ipvanish-cli --help
usage: ipvanish-cli [-h] [-i] [-d] [-r] [-k] [-f FQDN] [--city CITY]
                    [--country COUNTRY] [-c]

Launches openvpn client to IPVansish FQDN or if city and/or country are used,
lowest capacity server is slected from group.

optional arguments:
  -h, --help            show this help message and exit
  -i, --identify        Returns current external IP. Query
                        http://checkip.amazonaws.com/
  -d, --download        Downloads configs from ipvanish.
  -r, --report          Display list IPVanish servers - Use city and country
                        flags to narrow scope
  -k, --kill            Kills any running openvpn process
  -f FQDN, --FQDN FQDN  Selects server with FQDN
  --city CITY           Select all servers from city
  --country COUNTRY     Select all servers from country
  -c, --connect         Connects to selected server with lowest capacity. If
                        FQDN is provided connect there

```

# Install
```bash
    $ git clone https://code-serpent@bitbucket.org/code-serpent/ipvanish-cli.git
    $ cd ipvanish-cli
    $ pip3 install . --user
    $ ipvanish-cli -i
    # Public IP should be printed
```

# Config / Setup
This only needs to run on new installs.

*   This leverages keyring to access password.
*   If you do these steps and you uninstall using pip. Please remove the contents of the configs dir. Check for ipvanish_cli under site packages. 
```bash
    # Create login.conf
    $ ipvanish-cli --login
    Username: <IPVanish Username>
    Password: <IPVanish Password>

    # Download ipvanish's opvn files and cert.
    # Rerun periodicly as ipvanish is always standing up new servers.
    $ sudo ipvanish-cli --download

```
You are now ready to use the tool. 

# Examples

Generating report to see current capacity for all IPVanish servers in Atlanta
```bash
    $ ipvanish-cli --report --city Atlanta | head
    FQDN: atl-a76.ipvanish.com
    Country        City     FQDN                    Capacity  Online
    -------------  -------  --------------------  ----------  --------
    United States  Atlanta  atl-a76.ipvanish.com           0  True
    United States  Atlanta  atl-a21.ipvanish.com          30  True
    United States  Atlanta  atl-a05.ipvanish.com          31  True
    United States  Atlanta  atl-a57.ipvanish.com          31  True
    United States  Atlanta  atl-a43.ipvanish.com          32  True
    United States  Atlanta  atl-c01.ipvanish.com          32  True
    United States  Atlanta  atl-a53.ipvanish.com          32  True

```

Generating report to see current capacity for all IPvanish servers in the United States
```bash
    $ ipvanish-cli --report --country 'United States' | head
    FQDN: atl-a76.ipvanish.com
    Country        City            FQDN                    Capacity  Online
    -------------  --------------  --------------------  ----------  --------
    United States  Atlanta         atl-a76.ipvanish.com           0  True
    United States  Dallas          dal-a54.ipvanish.com           0  True
    United States  Milwaukee       mke-c19.ipvanish.com          14  True
    United States  Milwaukee       mke-c15.ipvanish.com          16  True
    United States  Milwaukee       mke-c07.ipvanish.com          17  True
    United States  Los Angeles     lax-a44.ipvanish.com          18  True
    United States  Salt Lake City  slc-c04.ipvanish.com          18  True

```

Generating report to see current capacity for specific FQDN.
```bash
    $ ipvanish-cli --report --FQDN mke-c19.ipvanish.com
    FQDN: mke-c19.ipvanish.com
    Country        City       FQDN                    Capacity  Online
    -------------  ---------  --------------------  ----------  --------
    United States  Milwaukee  mke-c19.ipvanish.com          14  True

```

Connecting to specific FQDN
```bash
    $ ipvanish-cli --connect --FQDN lax-a42.ipvanish.com
    FQDN: lax-a42.ipvanish.com
    b"Sat Apr 20 12:10:57 2019 WARNING: --keysize is DEPRECATED and will be removed in OpenVPN 2.6
    ...
    ...
    ...
    Sat Apr 20 12:10:59 2019 Initialization Sequence Completed
    "
    External IP changed.
    Original IP =	<old_ip>
    New IP 	=	<new_ip>



    To kill this process copy the following line.
    sudo kill 31802

```

Connecting to lowest capacity server in city or country. 

*   Sometimes the lowest ones in a city or country don't work. In that case generate report for that city or country and connect to the next lowest one with --FQDN option/flag

```bash
    $ ipvanish-cli --connect --city Seattle
    # or
    $ ipvanish-cli --connect --country 'United States'


```

Kill openvpn sessions.

*   Run this multiple times until you see output stating nothing was running.
```bash
    $ ipvanish-cli --kill
    Closed 31802.
    $ ipvanish-cli --kill
    Closed 31802.
    $ ipvanish-cli --kill
    No openvpn processes running. Exitting...

```

Get current external IP
```bash
    $ ipvanish-cli --identify
    <Current_External_IP>

```

Hope you all enjoy let me know about additional feature. Code still needs refactoring. I will get to that eventually.

Version: 2.0.1
Link:    [ipvanish-cli](https://bitbucket.org/code-serpent/ipvanish-cli/)


# Maintainer
Anthony.Aguilar@code-serpent.com

